## Wunschzettel
* Bücher ...
  * Paula Hawkins
    * A slow fire burning
    * The blue hour
  * Jasmin Schreiber
    * Alle Bücher außer _Marianengraben_
  * Natalie Knapp
    * Der unendliche Augenblick
  * Renee DiResta
    * Invisible Rulers
* Wasserfeste und warme Handschuhe fürs Fahrrad
* Einladungen zum Essen,Kaffeetrinken
* Bedrucktes T-Shirt
